function showCarData() {
  var htmlData = "";
  data = carsData.carFilterFunction();
  for (let index = 0; index < data.length; index++) {
    var car = data[index];
    htmlData += `
      <div class="col-auto m-2 mx-auto">
        <div class="card" style="width: 25rem;">
          <img class="card-img-top-img-fluid" src="${car.image}" alt="${car.manufacture}"  style="height: 250px; object-fit: cover;">
          <div class="card-body" style="font-size: 14px;">
            <p>${car.manufacture} | ${car.model}</p>
            <h5>Rp. ${car.rentPerDay} / hari</h5>
            <p style="text-align: justify; height:70px;"  class="w-75">${car.description}</p>
            <div class="car-detail my-4 ">
              <div class="detail-item"><img class="mx-2 my-2" src="./image/fi_users.png" />${car.capacity} Orang
              </div>
              <div class="detail-item"><img class="mx-2 my-2" src="./image/fi_settings.png" />${car.transmission}
              </div>
              <div class="detail-item"><img class="mx-2 my-2" src="./image/fi_calendar.png" />Tahun ${car.year}
              </div>
              </div>
              <div class="d-grid gap-2">
                <button type="button"
                class="btn btn-success mx-2 mt-2"
                style="background-color: #5cb85f">Pilih Mobil
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>`;
  }
  app.innerHTML = htmlData;
}
var btnCarSearch = document
  .getElementById("btnCari")
  .addEventListener("click", showCarData);
