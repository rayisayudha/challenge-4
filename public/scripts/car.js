var xmlHttp = new XMLHttpRequest();
xmlHttp.open("GET", "http://localhost:8000/api/cars", false);
xmlHttp.send(null);

var data = JSON.parse(xmlHttp.responseText);
class Car {
  constructor(carsData) {
    this.carsData = carsData;
  }
  filterCarByAvailability() {
    return this.carsData.filter((car) => car.available === true);
  }
  carFilterFunction() {
    const date = document.getElementById("date").value;
    const chooseDriver = document.getElementById("chooseDriver").value;
    const time = document.getElementById("time").value;
    const dateTime = date + time;
    const passanger = document.getElementById("passanger").value;

    if (chooseDriver === "" || date === "" || time === "") {
      alert("Please Fill All Required Fields!");
      return;
    } else {
      return this.carsData.filter(
        (car) =>
          car.available === true &&
          car.availableAt <= dateTime &&
          car.capacity >= passanger
      );
    }
  }
}
var carsData = new Car(data);
var app = document.getElementById("carData");
htmlData = "";
var data = cars.filterCarByAvailability();
